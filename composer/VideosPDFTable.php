<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Media_Plugin_VideosPDFTable extends Zend_Db_Table
{
    protected $_name = 'Media_videosPDF';

    public function addFile ($path, $filetype)
    {
        // Get all fields
        $hash       = md5_file ($path);
        $name       = basename ($path);
        $size       = filesize ($path);
        $preview    = $this->_getPreview ($path);
        $length     = $this->_getLength ($path);

        if ($this->fileExists ($hash))
        {       
            error_log("File already exists (hash $hash)");
            return (NULL);
        }

        $hashConfirm = $this->insert (
            array (
                'hash'      => $hash,
                'name'      => $name,
                'path'      => $path,
                'pdforigin' => $path,
                'size'      => $size,
                'length'    => $length,
                'preview'   => $preview
                                     ));
        return ($hashConfirm);
    }

    public function delFile ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();

        // Remove the PDF and the Video from the disk
        if (file_exists ($row ['path']))
            unlink ($row ['path']);
        if (file_exists ($row ['pdforigin']))
            unlink ($row ['pdforigin']);

        // Remove the file from the DB
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);

        return (true);
    }

    public function getFiles ()
    {
        $result = array ();
        $allVideosPDF = $this->fetchAll ($this->select ()->order ('name'));
        foreach ($allVideosPDF as $pdf)
            $result [] = $pdf->toArray ();

        return ($result);
    }

    public function getLengthInSeconds ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
            return (0);

        list ($hours, $minutes, $seconds) = explode (':', $row ['length']);
        return ($hours*3600 + $minutes*60 + $seconds);
    }

    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        pclose ($f);

        return (trim ($result));
    }

    public function getFilePath ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return ($row['path']);
    }

    public function getPdfFilePath ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return ($row['pdforigin']);
    }

    public function getPreview ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        return ($row ['preview']);
    }

    public function fileExists ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return (true);
    }

    protected function _getPreview ($path)
    {
        $filename   = basename  ($path);
        $basepath   = dirname   ($path);
        $extension  = strrchr   ($path, '.');
        $basefname  = substr    ($filename, 0, -strlen ($extension));

        $basefname = str_replace (" ", "\\ ", $basefname);

        $thumbPath = "$basepath/.$basefname.png";
        $srcImage = imageCreateFromPNG ($thumbPath);
        $old_x = imageSX ($srcImage);
        $old_y = imageSY ($srcImage);
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $thumb_w = $upload->preview_size;
        $thumb_h = $upload->preview_size;
        $previewImage = ImageCreateTrueColor ($thumb_w, $thumb_h);
        imageCopyResampled ($previewImage, $srcImage,
                            0, 0, 0, 0,
                            $thumb_w, $thumb_h, $old_x, $old_y);
        if (imagepng ($previewImage, '/tmp/tmp_preview.png'))
            $preview = file_get_contents ('/tmp/tmp_preview.png');
        else
            $preview = NULL;
        imagedestroy ($previewImage);
        unlink ($thumbPath);
        return ($preview);
    }

    protected function _getLength ($path)
    {
        // Count the number of pictures (1/page)
        $filename   = basename  ($path);
        $basepath   = dirname   ($path);
        $extension  = strrchr   ($path, '.');
        $basefname  = substr    ($filename, 0, -strlen ($extension));

        $basefname = str_replace (" ", "\\ ", $basefname);
        $cmd = "ls -l $basepath/.$basefname-???.png | wc -l";
        $nbPages = $this->_execute ($cmd);

        $hours      = (int)($nbPages / 3600);
        $minutes    = (int)(($nbPages % 3600) / 60);
        $seconds    = $nbPages % 60;

        return ("$hours:$minutes:$seconds");
    }
}
