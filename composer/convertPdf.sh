#!/bin/bash
# Convert PDF to Video

FILE=/usr/bin/file
GS=/usr/bin/gs
IDENTIFY=/usr/bin/identify
COMPOSITE=/usr/bin/composite
CONVERT=/usr/bin/convert
MAXDIMENSIONS=1920
AVCONV=/usr/bin/avconv

function iInfo
{
    logger -p local4.info -t PDF -- "$@"
    echo "$@"
}
function iError
{
    logger -p local4.err -t PDF -- "$@"
    echo "$@"
}

function usage
{
    echo "Usage: $0 <path> ['thumbnail'] [pause]"
    echo ""
    echo "  With:"
    echo "    <path>    : Path to the PDF file"
    echo "    <pause>   : Duration (seconds) on each PDF page"
    echo "                Default is 5 seconds"
    echo "    thumbnail : Create thumbnail and exit"
    echo ""
}

#==============================================================================
# Create thumbnail
function createThumbnail
{
    THUMBNAIL=$( echo ".${PDF_FILE%.*}.png" )
    RESULT=$( ${GS} -sDEVICE=png16m -dSAFER -dBATCH -dNOPAUSE -r72 \
                -dFirstPage=1 -dLastPage=1 \
                -o "${PDF_DIR}/${THUMBNAIL}" "${PDF_PATH}" )

    if [ "$?" != "0" ] ; then
        iError "Failed to create thumbnail."
        iError "${RES}"
        return 1
    fi
}

# Get PATH of the generated video
function getVideoPath
{
    VIDEO_PATH="${PDF_PATH%.*}"
    echo "${VIDEO_PATH}_pdf_${PDF_PAUSE}.mp4"
}

function getNbPages
{
    # Get number of pages
    NB_PAGES=$( ${GS} -q -dNODISPLAY \
                -c "(${PDF_PATH}) (r) file runpdfbegin pdfpagecount = quit" )
    export NB_PAGES
    iInfo "PDF length: ${NB_PAGES} pages."
}

# Convert PDF into PNG files (one PNG per PDF page)
function convertPdfToPictures
{
    BASENAME=$( basename ${PDF_PATH%.*} )

    BASEPATH="${WORKSPACE_DIR}/${BASENAME}_%03d.png"

    # Create pictures
    RESULT=$( ${GS} -sDEVICE=png16m -dSAFER \
                    -dBATCH -dNOPAUSE -r600 \
                    -o "${BASEPATH}" "${PDF_PATH}" )
    if [ "$?" != "0" ] ; then
        iError "Unable to create pictures from PDF."
        iError "${RESULT}"
        return 1
    fi

    # Double the first picture
    FIRST=$( echo ${BASEPATH} | sed -r 's/_%03d/_001/g' )
    export INTRO=$( echo ${BASEPATH} | sed -r 's/_%03d/_000/g' )
    cp -f "${FIRST}" "${INTRO}"

    return 0
}

function resizePictures
{
    # Resize pictures so that the greatest dimension is max 1920px

    # Check first image exists
    if [ ! -f "${INTRO}" ] ; then
        return 1
    fi

    # Get Height and Width dimensions
    DIMENSIONS=$( getPictureDimensions "${INTRO}" )
    WIDTH=$( echo "${DIMENSIONS}" | cut -d'x' -f1 )
    HEIGHT=$( echo "${DIMENSIONS}" | cut -d'x' -f2 )

    iInfo "Original dimensions: ${WIDTH}x${HEIGHT}"

    # Check if some images in the list have greater dimensions
    PICTURES_LIST=$( echo ${BASEPATH} | \
                            sed -r 's/_%03d/_[0-9]*/g' | \
                            sed -r 's/ /\\\\ /g' )

    for i in ${PICTURES_LIST} ; do
        NEW_DIMENSIONS=$( getPictureDimensions "${i}" )
        NEW_WIDTH=$( echo "${NEW_DIMENSIONS}" | cut -d'x' -f1 )
        NEW_HEIGHT=$( echo "${NEW_DIMENSIONS}" | cut -d'x' -f2 )

        if [ "${NEW_WIDTH}" -gt "${WIDTH}" ] ; then
            WIDTH="${NEW_WIDTH}"
        fi
        if [ "${NEW_HEIGHT}" -gt "${HEIGHT}" ] ; then
            WIDTH="${NEW_HEIGHT}"
        fi
    done
    iInfo "Maximal dimensions: ${WIDTH}x${HEIGHT}"

    if [ "${WIDTH}" -gt "${HEIGHT}" ] ; then
        # Orientation PDF : Paysage
        # Maximum width is 1920
        iInfo "Maximal width: ${MAXDIMENSIONS}"
        SCALE=$( echo "scale=0; ${WIDTH}*100/${MAXDIMENSIONS}" | bc )
    else
        # Orientation PDF : Portrait
        # Maximum height is 1920
        iInfo "Maximal height: ${MAXDIMENSIONS}"
        SCALE=$( echo "scale=0; ${HEIGHT}*100/${MAXDIMENSIONS}" | bc )
    fi

    # Re-scale only if dimensions are greater
    if [ "${SCALE}" -le "100" ] ; then
        # Nothing to do
        return 0
    fi
    SCALE=$(( ${SCALE} + 1 ))

    SCALED_WIDTH=$( echo "scale=0; (${WIDTH}*100)/${SCALE}" | bc )
    SCALED_HEIGHT=$( echo "scale=0; (${HEIGHT}*100)/${SCALE}" | bc )

    # Dimensions shall be multiple of 2
    SCALED_WIDTH=$( echo "(${SCALED_WIDTH} / 2) * 2" | bc )
    SCALED_HEIGHT=$( echo "(${SCALED_HEIGHT} / 2) * 2" | bc )

    # Create Canvas from first picture, with greatest dimensions,
    # and fill in black color
    export CANVAS=$( echo ${BASEPATH} | sed -r 's/_%03d/_canvas/g' )
    RESULT=$( ${CONVERT} -size ${SCALED_WIDTH}x${SCALED_HEIGHT} \
                         xc:black \
                         "${CANVAS}" )
    if [ "$?" != "0" ] ; then
        iError "Cannot create Canvas."
        iError "${RESULT}"
        return 1
    fi

    # Scale and center each picture with canvas as model
    iInfo "Resizing generated pictures with dimensions ${SCALED_WIDTH}x${SCALED_HEIGHT}"
    for i in ${PICTURES_LIST} ; do
        # Every image shall be resized with same ratio (dimensions in pixel can vary)
        OLD_DIMENSIONS=$( getPictureDimensions "${i}" )
        OLD_WIDTH=$( echo "${OLD_DIMENSIONS}" | cut -d'x' -f1 )
        OLD_HEIGHT=$( echo "${OLD_DIMENSIONS}" | cut -d'x' -f2 )

        # Resize + Dimensions shall be multiple of 2
        SCALED_WIDTH=$( echo "scale=0; (${OLD_WIDTH}*100)/${SCALE}" | bc )
        SCALED_HEIGHT=$( echo "scale=0; (${OLD_HEIGHT}*100)/${SCALE}" | bc )
        SCALED_WIDTH=$( echo "(${SCALED_WIDTH} / 2) * 2" | bc )
        SCALED_HEIGHT=$( echo "(${SCALED_HEIGHT} / 2) * 2" | bc )

        RESULT=$( ${CONVERT} "$i" -resize ${SCALED_WIDTH}x${SCALED_HEIGHT} "${i}.resized" )
        if [ "$?" != "0" ] ; then
            iError "Cannot resize image '$i'."
            iError "${RESULT}"
            return 1
        fi

        # Center picture with black background
        RESULT=$( ${COMPOSITE} -gravity center "${i}.resized" "${CANVAS}" "$i" )
        if [ "$?" != "0" ] ; then
            iError "Cannot center image '$i'."
            iError "${RESULT}"
            return 1
        fi
        rm -f ${i}.resized

    done

    # Remove canvas image to not be used by the Video
    rm -f "${CANVAS}"

    return 0
}

# Convert Pictures to Video
function convertPictureToVideo
{
    FILEMATCH=$( echo ${BASEPATH} | sed -r 's/ /\\\\ /g' )
    RESULT=$( ${AVCONV} -r 1/${PDF_PAUSE} \
                -i ${FILEMATCH} -y "${VIDEO_PATH}" 2>&1 )
    if [ "$?" != "0" ] ; then
        iError "Failed to create the Video."
        iError "${RESULT}"
        return 1
    fi

    # Clean temporary workspace, with pictures
    rm -rf "${WORKSPACE_DIR}"

    return 0
}

# Update Composer's database with the generated video filename
function updateDB
{
    USER="composer"
    PASS="%-composer-%"
    DB="iComposer"
    TABLE="Media_videosPDF"

    HASH=$( md5sum ${PDF_PATH} | cut -d' ' -f1 )
    LENGTH=$( echo "${NB_PAGES} * ${PDF_PAUSE}" | bc )
    LENGTH_HOUR=$( echo $( printf %02.0f $( echo "scale=0; ${LENGTH}/3600" | bc )))
    LENGTH_MIN=$( echo $( printf %02.0f $( echo "scale=0; ${LENGTH}/60" | bc )))
    LENGTH_SEC=$( echo $( printf %02.0f $( echo "scale=0; ${LENGTH} % 60" | bc )))

    CMD="UPDATE ${TABLE} SET path='${VIDEO_PATH}',length='${LENGTH_HOUR}:${LENGTH_MIN}:${LENGTH_SEC}' WHERE hash='${HASH}'"

    RES=$( mysql -u "${USER}" -p"${PASS}" -D "${DB}" \
            -e "${CMD}" 2>&1 )

    return $?
}

# Return dimensions of given picture as <'width'x'height'>
function getPictureDimensions
{
    DIMENSIONS=$( file $1 | \
                    sed -r 's/^.*, ?([0-9]+) ?x ?([0-9]+) ?,.*/\1x\2/g' )
    echo "${DIMENSIONS}"
}

#==============================================================================
# Main procedure


# Check the parameters
if [ "$#" -lt 1 -o "$#" -gt 2 ];
then
    usage
    exit -1
fi

# Check PDF exists
export PDF_PATH=$1
export PDF_DIR=$( dirname "${PDF_PATH}" )
export PDF_FILE=$( basename "${PDF_PATH}" )
if [ ! -f "${PDF_PATH}" ] ; then
    iError "${PDF_PATH} does not exist."
    usage
    exit -1
fi

# Check the PDF is in PDF format
MIME=$( $FILE -L -b --mime-type "${PDF_PATH}" )
if [ "$MIME" != "application/pdf" ];
then
    iError "${PDF_PATH} is NOT a PDF file"
    exit -1
fi


# Configure the pause between 2 pages
if [ "$#" = "2" ]; then
    PDF_PAUSE=$2
else
    PDF_PAUSE=5
fi
export PDF_PAUSE

export VIDEO_PATH=$( getVideoPath "${PDF_PATH}" )

export WORKSPACE_DIR=$( mktemp -d )

if [ "${PDF_PAUSE}" = "thumbnail" ] ; then
    # Create thumbnail
    createThumbnail || exit 1
    exit 0
fi

getNbPages

# Create pictures from PDF
convertPdfToPictures || exit -1

# Resize pictures (maximum allowed is HD)
resizePictures || exit -1

# Create video from pictures
convertPictureToVideo || exit -1

# Update information in Database (set correct video's name)
updateDB || exit -1

exit 0
