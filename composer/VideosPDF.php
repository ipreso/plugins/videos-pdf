<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
require_once 'Media/Plugin/Skeleton.php';
require_once 'Media/Plugin/VideosPDFTable.php';

class Media_Plugin_VideosPDF extends Media_Plugin_Skeleton
{
    protected $_tableObj;
    private $_pausePerPage = 5;
    private $_convertBin = "/usr/bin/convertPdf.sh";

    public function __construct ()
    {
        $this->_name        = 'VideosPDF';
        $this->_tableObj    = new Media_Plugin_VideosPDFTable ();
    }

    public function getName ()
    {
        return ($this->getTranslation ('Pdf'));
    }

    public function areItemsDeletable () { return (true); }
    public function areItemsDownloadable () { return (true); }

    public function isManagingFile ($path)
    {
        $ext = strrchr ($path, '.');
        if (strncasecmp ($ext, ".pdf", strlen ($ext)) == 0)
            return (true);
        else
            return (false);
    }

    public function addFile ($path)
    {
        // Create thumbnail, then
        system ($this->_convertBin." \"$path\" thumbnail");
        $hash = $this->_tableObj->addFile ($path, $this->_getType ($path));

        // Then, resize PDF if too big and composer Video from PDF (in background)
        system ("/bin/bash -c '/usr/bin/nohup ".$this->_convertBin." \"$path\" ".$this->_pausePerPage." &>/dev/null &'");
        return ($hash);
    }

    public function delFile ($hash)
    {
        return ($this->_tableObj->delFile ($hash));
    }

    public function getItems ()
    {
        $result = array ();

        $pdfs = $this->_tableObj->getFiles ();
        foreach ($pdfs as $pdf)
        {
            $length = $this->_tableObj->getLengthInSeconds ($pdf ['hash']);
            
            $hours      = (int)($length / 3600);
            $minutes    = (int)(($length % 3600) / 60);
            $seconds    = $length % 60;

            $result [$pdf ['hash']] =
                array (
                    'name'      => $pdf ['name'],
                    'length'    => "$hours:$minutes:$seconds"
                      );
        }

        return ($result);
    }

    public function getPreview ($hash)
    {
        return ($this->_tableObj->getPreview ($hash));
    }

    public function fileExists ($hash)
    {
        return ($this->_tableObj->fileExists ($hash));
    }

    public function getFilePath ($hash)
    {
        // Get the PDF file (called by 'Download' button on Catalog page)
        return ($this->_tableObj->getPdfFilePath ($hash));
    }

    public function getDefaultStartDate ()
    {
        // No default startdate
        return (NULL);
    }

    public function getDefaultEndDate ()
    {
        // No default enddate
        return (NULL);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Video properties are :
        // - duration
        // - start
        // - end

        $duration   = 0;

        $defaultVideoProperties = new Media_Property ();
        $defaultVideoProperties->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration,
                                        $this->getTranslation ('Seconds (0 = until the end)')),
                   $this->createDateProperty (
                                'startdate',
                                $this->getTranslation ('Start'),
                                "",
                                'Media is enables from this date'),
                   $this->createDateProperty (
                                'enddate',
                                $this->getTranslation ('End'),
                                "",
                                'Media is played until this date')));
        return ($defaultVideoProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $value, 
                                        $this->getTranslation ('Seconds (0 = until the end)'));
                    break;
                case "startdate":
                    $property = $this->createDateProperty (
                                    'startdate',
                                    $this->getTranslation ('Start'), 
                                    $value,
                                    'Media is enables from this date');
                    break;
                case "enddate":
                    $property = $this->createDateProperty (
                                        'enddate',
                                        $this->getTranslation ('End'), 
                                        $value,
                                        'Media is played until this date');
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function getMD5 ($item)
    {
        $IDs = $item->getSequenceProperties ();

        // Get path of the Video from the PDF
        $generatedvideopath = $this->_tableObj->getFilePath ($IDs ['id']);
        if (!$generatedvideopath)
            return (array());
        
        // Return MD5 and name of the generated Video
        return (array (
                    array ('hash'   => md5_file ($generatedvideopath),
                           'file'   => basename ($generatedvideopath))));
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Convert filename to the generated video's one (instead of PDF file)
        $generatedvideopath = $this->_tableObj->getFilePath ($IDs ['id']);
        $videoname = basename ($generatedvideopath);

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/$videoname:";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "startdate":
                    $line .= " startdate=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "enddate":
                    $line .= " enddate=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        // Add random string to manage multiple PDF with same parameters
        $line .= " rdm=\"".rand(0,99999999)."\"";
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = 0;

        // - startdate
        if (preg_match ('/.*startdate="([0-9\-]+)".*/', $line, $matches))
            $startdate = $matches [1];
        else
            $startdate = '';

        // - enddate
        if (preg_match ('/.*enddate="([0-9\-]+)".*/', $line, $matches))
            $enddate = $matches [1];
        else
            $enddate = '';

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration, 
                                        $this->getTranslation ('Seconds (0 = until the end)')),
                   $this->createDateProperty (
                                        'startdate',
                                        $this->getTranslation ('Start'), 
                                        $startdate,
                                        'Media is enables from this date'),
                   $this->createDateProperty (
                                        'enddate',
                                        $this->getTranslation ('End'), 
                                        $enddate,
                                        'Media is played until this date')));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        $hash       = $propObj->getItem ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    $duration = $this->getTextValue ($property);
                    if ($duration == "0")
                    {
                        $length = $this->_tableObj->getLengthInSeconds ($hash);
                        return ($length);
                    }
                    else
                        return ($duration);
                case "startdate":
                    return ($this->getTextValue ($property));
                case "enddate":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }

    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        pclose ($f);

        return (trim ($result));
    }
}
